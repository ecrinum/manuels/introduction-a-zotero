# Introduction à Zotero
Support pour un cours à destination d'étudiants de l'Université de Montréal.

## Présentations en ligne
[https://ecrinum.gitpages.huma-num.fr/manuels/introduction-a-zotero/](https://ecrinum.gitpages.huma-num.fr/manuels/introduction-a-zotero/)

## Support
Le support est disponible ici sous la licence Creative Commons CC BY-SA et au format Markdown (fichier `index.md`).
